# 🌞 Installer MariaDB sur la machine db.tp2.cesi

```
[tony@db ~]$ sudo dnf install mariadb-server
Last metadata expiration check: 4:39:35 ago on Tue 14 Dec 2021 10:26:37 AM CET.
Dependencies resolved.
======================================================================================================================================
 Package                              Architecture     Version                                              Repository           Size
======================================================================================================================================
Installing:
 mariadb-server                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream            16 M
Installing dependencies:
 mariadb                              x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream           6.0 M
 mariadb-common                       x86_64           3:10.3.28-1.module+el8.4.0+427+adf35707              appstream            62 k
 mariadb-connector-c                  x86_64           3.1.11-2.el8_3                                       appstream           199 k
 
 [...]
 
   perl-Time-Local-1:1.280-1.el8.noarch                                  perl-URI-1.73-3.el8.noarch
  perl-Unicode-Normalize-1.25-396.el8.x86_64                            perl-constant-1.33-396.el8.noarch
  perl-interpreter-4:5.26.3-420.el8.x86_64                              perl-libnet-3.11-3.el8.noarch
  perl-libs-4:5.26.3-420.el8.x86_64                                     perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch                                      perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64                                      perl-threads-shared-1.58-2.el8.x86_64
  psmisc-23.1-5.el8.x86_64

Complete!
 ```


# 🌞 Le service MariaDB

```
[tony@db ~]$ systemctl status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-14 15:13:35 CET; 15min ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 26352 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 26217 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 26193 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 26320 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11221)
   Memory: 87.8M
   CGroup: /system.slice/mariadb.service
           └─26320 /usr/libexec/mysqld --basedir=/usr

Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: See the MariaDB Knowledgebase at http://mariadb.com/kb or the
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: MySQL manual for more instructions.
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: Please report any problems at http://mariadb.org/jira
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: The latest information about MariaDB is available at http://mariadb.org/.
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: You can find additional information about the MySQL part at:
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: http://dev.mysql.com
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: Consider joining MariaDB's strong and vibrant community:
Dec 14 15:13:35 db.tp2.cesi mysql-prepare-db-dir[26217]: https://mariadb.org/get-involved/
Dec 14 15:13:35 db.tp2.cesi mysqld[26320]: 2021-12-14 15:13:35 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB) starting as proc>
Dec 14 15:13:35 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.                                                                                         loaded active running   MariaDB 10.3 database server
[tony@db ~]$ ss -lutpn
Netid        State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
tcp          LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
tcp          LISTEN        0             80                               *:3306                          *:*
tcp          LISTEN        0             128                           [::]:22                         [::]:*
[tony@db ~]$ ps -ef | grep maria
tony       26524    1588  0 15:24 pts/0    00:00:00 grep --color=auto maria
```


# 🌞 Firewall

```
[tony@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for tony:
success
```

# 🌞 Configuration élémentaire de la base

```
[tony@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

[...]

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

# 🌞 Préparation de la base en vue de l'utilisation par NextCloud

```
[tony@db ~]$ sudo mysql -u root -p
[sudo] password for tony:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create user 'nextcloud'@'10.2.1.11' identified by 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> create database if not exists nextcloud character set utf8mb4 collate utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> grant all privileges on nextcloud.* to 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> flush privileges;
Query OK, 0 rows affected (0.001 sec)
```

# 🌞 Installez sur la machine web.tp2.cesi la commande mysql#

```
[tony@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                                                             2.1 MB/s | 8.4 MB     00:03
Rocky Linux 8 - BaseOS                                                                                1.5 MB/s | 3.6 MB     00:02
Rocky Linux 8 - Extras                                                                                5.7 kB/s |  10 kB     00:01
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
[tony@web ~]$ sudo dnf install mysql
Last metadata expiration check: 0:08:08 ago on Tue 14 Dec 2021 03:42:33 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                               Architecture      Version                                           Repository            Size
======================================================================================================================================
Installing:
 mysql                                 x86_64            8.0.26-1.module+el8.4.0+652+6de068a7              appstream             12 M
Installing dependencies:
 mariadb-connector-c-config            noarch            3.1.11-2.el8_3                                    appstream             14 k
 mysql-common                          x86_64            8.0.26-1.module+el8.4.0+652+6de068a7              appstream            133 k
Enabling module streams:
 mysql                                                   8.0
 
 [...]
 
   Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                                             1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                                            2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64  


  3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                      mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

# 🌞 Tester la connexion

```
[tony@web ~]$ sudo mysql -h 10.2.1.12 -u nextcloud -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 20
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use nextcloud;
Database changed
mysql> show tables;
Empty set (0.00 sec)

```



# 🌞 Installer Apache sur la machine web.tp2.cesi

```
[tony@web ~]$ sudo dnf install httpd
Last metadata expiration check: 17:07:22 ago on Tue 14 Dec 2021 03:42:33 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                        Architecture        Version                                              Repository              Size
======================================================================================================================================
Installing:
 httpd                          x86_64              2.4.37-43.module+el8.5.0+714+5ec56ee8                appstream              1.4 M
Installing dependencies:
 apr                            x86_64              1.6.3-12.el8                                         appstream              128 k
 
 [...]
 
   Verifying        : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                                                6/9
  Verifying        : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                                     7/9
  Verifying        : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                                        8/9
  Verifying        : rocky-logos-httpd-85.0-3.el8.noarch                                                                          9/9

Installed:
  apr-1.6.3-12.el8.x86_64                                         apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64                               apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64              httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64        mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

# 🌞 Analyse du service Apache

```
[tony@web ~]$ service httpd start
Redirecting to /bin/systemctl start httpd.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: tony
Password:
==== AUTHENTICATION COMPLETE ====
[tony@web ~]$ service httpd status
Redirecting to /bin/systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-15 08:56:24 CET; 16s ago
     Docs: man:httpd.service(8)
 Main PID: 2149 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11221)
   Memory: 26.1M
   CGroup: /system.slice/httpd.service
           ├─2149 /usr/sbin/httpd -DFOREGROUND
           ├─2150 /usr/sbin/httpd -DFOREGROUND
           ├─2151 /usr/sbin/httpd -DFOREGROUND
           ├─2152 /usr/sbin/httpd -DFOREGROUND
           └─2153 /usr/sbin/httpd -DFOREGROUND

Dec 15 08:56:14 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 15 08:56:24 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 15 08:56:30 web.tp2.cesi httpd[2149]: Server configured, listening on: port 80
[tony@web ~]$ ps -ef | grep apache
apache      2150    2149  0 08:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2151    2149  0 08:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2152    2149  0 08:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2153    2149  0 08:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
tony        2419    1602  0 09:02 pts/0    00:00:00 grep --color=auto apache
```

# 🌞 Un premier test

```
[tony@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[tony@web ~]$ sudo firewall-cmd --reload
success
[tony@web ~]$ curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  
  [...]
  
            <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```

# 🌞 Installer PHP

```
[tony@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:30 ago on Wed 15 Dec 2021 09:15:56 AM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
======================================================================================================================================
 Package                                 Architecture      Version                                         Repository            Size
======================================================================================================================================
Installing:
 php74-php                               x86_64            7.4.27-1.el8.remi                               remi-safe            1.5 M
 php74-php-bcmath                        x86_64            7.4.27-1.el8.remi                               remi-safe             88 k
 
 [...]
 
   php74-php-opcache-7.4.27-1.el8.remi.x86_64                            php74-php-pdo-7.4.27-1.el8.remi.x86_64
  php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                           php74-php-process-7.4.27-1.el8.remi.x86_64
  php74-php-sodium-7.4.27-1.el8.remi.x86_64                             php74-php-xml-7.4.27-1.el8.remi.x86_64
  php74-runtime-1.0-3.el8.remi.x86_64                                   policycoreutils-python-utils-2.9-16.el8.noarch
  python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                python3-libsemanage-2.9-6.el8.x86_64
  python3-policycoreutils-2.9-16.el8.noarch                             python3-setools-4.3.0-2.el8.x86_64
  scl-utils-1:2.0.2-14.el8.x86_64                                       tcl-1:8.6.8-2.el8.x86_64

Complete!
 ```

# 🌞 Analyser la conf Apache

```
[tony@web ~]$ cat /etc/httpd/conf/httpd.conf  | grep conf.d
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

# 🌞 Créer un VirtualHost qui accueillera NextCloud

```
[tony@web ~]$ cat /etc/httpd/conf.d/creationVirtualHost.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[tony@web ~]$ sudo systemctl restart httpd
[tony@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
  Drop-In: /usr/lib/systemd/system/httpd.service.d
           └─php74-php-fpm.conf
   Active: active (running) since Wed 2021-12-15 09:56:03 CET; 16s ago
     Docs: man:httpd.service(8)
 Main PID: 15184 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11221)
   Memory: 24.7M
   CGroup: /system.slice/httpd.service
           ├─15184 /usr/sbin/httpd -DFOREGROUND
           ├─15189 /usr/sbin/httpd -DFOREGROUND
           ├─15190 /usr/sbin/httpd -DFOREGROUND
           ├─15191 /usr/sbin/httpd -DFOREGROUND
           └─15192 /usr/sbin/httpd -DFOREGROUND

Dec 15 09:55:52 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 15 09:55:52 web.tp2.cesi httpd[15184]: AH00112: Warning: DocumentRoot [/var/www/nextcloud/html/] does not exist
Dec 15 09:56:03 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 15 09:56:08 web.tp2.cesi httpd[15184]: Server configured, listening on: port 80
```

# 🌞 Configurer la racine web

```
[tony@web ~]$ sudo mkdir /var/www/nextcloud/
[tony@web ~]$ sudo mkdir /var/www/nextcloud/html/
[tony@web ~]$ sudo chown -R apache /var/www/nextcloud/html/
```

# 🌞 Configurer PHP

```
[tony@web ~]$ sudo cat /etc/opt/remi/php74/php.ini | grep date.timezone -n
922:; http://php.net/date.timezone
923:;date.timezone = Europe/Paris (CET, +0100)
```

# 🌞 Récupérer Nextcloud

```
[tony@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:--  0:00:01 --:--:--     0
100  148M  100  148M    0     0  6153k      0  0:00:24  0:00:24 --:--:-- 21.6M
[tony@web ~]$ ls
nextcloud-21.0.1.zip
```

# 🌞 Ranger la chambre

```
[tony@web ~]$ unzip nextcloud-21.0.1.zip

[...]

   creating: nextcloud/3rdparty/patchwork/jsqueeze/src/
  inflating: nextcloud/3rdparty/patchwork/jsqueeze/src/JSqueeze.php
  inflating: nextcloud/3rdparty/patchwork/jsqueeze/LICENSE.ASL20
  inflating: nextcloud/3rdparty/patchwork/jsqueeze/LICENSE.GPLv2
  inflating: nextcloud/3rdparty/patchwork/jsqueeze/composer.json
  inflating: nextcloud/3rdparty/patchwork/jsqueeze/README.md
  inflating: nextcloud/COPYING
   creating: nextcloud/config/
 extracting: nextcloud/config/CAN_INSTALL
  inflating: nextcloud/config/config.sample.php
  inflating: nextcloud/config/.htaccess
[tony@web ~]$ ls
nextcloud  nextcloud-21.0.1.zip
[tony@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/
```

# 🌞 Modifiez le fichier hosts de votre PC

```
# CESI TP Linux
10.2.1.11	web.tp2.cesi
```

# 🌞 Tester l'accès à NextCloud et finaliser son install'

```
[tony@web config]$ curl http://web.tp2.cesi
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>

[tony@web html]$ sudo -u apache php74 occ db:convert-type mysql nextcloud 10.2.1.12 nextcloud
The current PHP memory limit is below the recommended value of 512MB.
What is the database password?
Creating schema in new database
oc_accounts
 1/1 [============================] 100%oc_accounts_data
 6/6 [============================] 100%oc_activity
    0 [>---------------------------]oc_activity_mq
    0 [>---------------------------]oc_addressbookchanges
    0 [>---------------------------]oc_addressbooks
    0 [>---------------------------]oc_appconfig
 140/140 [============================] 100%oc_authtoken
 2/2 [============================] 100%oc_bruteforce_attempts
 
 [...]
 
  1/1 [============================] 100%oc_user_status
 1/1 [============================] 100%oc_user_transfer_owner
    0 [>---------------------------]oc_users
 1/1 [============================] 100%oc_vcategory
    0 [>---------------------------]oc_vcategory_to_object
    0 [>---------------------------]oc_webauthn
    0 [>---------------------------]oc_whats_new
    0 [>---------------------------][tony@web html]$
```

# 🌞 Modifier la conf du serveur SSH

```
PermitRootLogin no
# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication no

# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication yes

GSSAPIStrictAcceptorCheck yes
```

# 🌞 Installez et configurez fail2ban

```
[tony@web ~]$ sudo systemctl start firewalld
[sudo] password for tony:
[tony@web ~]$ sudo systemctl enable firewalld
[tony@web ~]$ sudo dnf install epel-release -y
Last metadata expiration check: 0:00:06 ago on Wed 15 Dec 2021 01:31:41 PM CET.
Package epel-release-8-13.el8.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[tony@web ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
Last metadata expiration check: 0:00:28 ago on Wed 15 Dec 2021 01:31:41 PM CET.
Dependencies resolved.
======================================================================================================================================
 Package                         Architecture        Version                                             Repository              Size
======================================================================================================================================
Installing:
 fail2ban                        noarch              0.11.2-1.el8                                        epel                    19 k
 fail2ban-firewalld              noarch              0.11.2-1.el8                                        epel                    19 k
Installing dependencies:

[...]

Installed:
  esmtp-1.2-15.el8.x86_64                fail2ban-0.11.2-1.el8.noarch                          fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch  fail2ban-server-0.11.2-1.el8.noarch                   libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64          python3-pip-9.0.3-20.el8.rocky.0.noarch               python3-setuptools-39.2.0-6.el8.noarch
  python3-systemd-234-8.el8.x86_64       python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!
[tony@web ~]$ sudo systemctl start fail2ban
[tony@web ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[tony@web ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[tony@web ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
[tony@web ~]$ sudo systemctl restart fail2ban
```

# 🌞 Installer NGINX

```
[tony@proxy ~]$ sudo dnf install nginx
Last metadata expiration check: 1 day, 3:37:53 ago on Tue 14 Dec 2021 10:26:37 AM CET.
Dependencies resolved.
======================================================================================================================================
 Package                               Architecture     Version                                             Repository           Size
======================================================================================================================================
Installing:
 nginx                                 x86_64           1:1.14.1-9.module+el8.4.0+542+81547229              appstream           566 k
Installing dependencies:
 fontconfig                            x86_64           2.13.1-4.el8                                        baseos              273 k
 gd                                    x86_64           2.2.5-7.el8                                         appstream           143 k
 jbigkit-libs                          x86_64           2.1-14.el8                                          appstream            54 k
 libX11                                x86_64           1.6.8-5.el8                                         appstream           610 k
 libX11-common                         noarch           1.6.8-5.el8                                         appstream           157 k
 libXau                                x86_64           1.0.9-3.el8                                         appstream            36 k
 
 [...]
 
   perl-libnet-3.11-3.el8.noarch
  perl-libs-4:5.26.3-420.el8.x86_64
  perl-macros-4:5.26.3-420.el8.x86_64
  perl-parent-1:0.237-1.el8.noarch
  perl-podlators-4.11-1.el8.noarch
  perl-threads-1:2.21-2.el8.x86_64
  perl-threads-shared-1.58-2.el8.x86_64

Complete!
```


# 🌞 Configurer NGINX comme reverse proxy

```
[tony@proxy ~]$ cat /etc/nginx/conf.d/reverseProxyconf.conf
server {
    listen 80;
    server_name web.tp2.cesi;

    location / {
       proxy_pass http://web.tp2.cesi:80;
    }
}
```

## fichier "hosts"

```
# CESI TP Linux
#10.2.1.11	web.tp2.cesi
10.2.1.13	web.tp2.cesi
```

> eeeeeeeeeeeeeeeeeeeet je suis resté bloqué sur le reverse-proxy o(TヘTo)